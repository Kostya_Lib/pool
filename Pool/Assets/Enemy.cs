﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Rigidbody rb;
    public static int EnemyGo;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Wall")
        {
            Pool.Instanse.Set("enemy");
            EnemyGo++;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "bullet")
        {
            Pool.Instanse.Set("enemy");
        }
    }
}
