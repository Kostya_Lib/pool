﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

[System.Serializable]
public class PoolObjects 
{
    public GameObject prefab;
    public int amount;

}

public class Pool : MonoBehaviour
{
    public List<PoolObjects> poolObjects;
    public List<GameObject> objectsOnScene;
    public GameObject StartGame;
    public GameObject Deadpanal;

    private bool Game = false;
    //[SerializeField, Header("")] Transform spawn;

    public static Pool Instanse;

    public static Faza faza;
    public enum Faza
    {
        StartГейма,
        GameПлей,
        YourСмерть

    }

    private void Awake()
    {
        if (Instanse == null)
        {
            Instanse = this;
        }
        else
        {
            Destroy(this);
        }

    }

    void Start()
     {
        foreach (PoolObjects item in poolObjects)
        {
            for (int i = 0; i < item.amount; i++)
            {
                
               GameObject obg =  Instantiate(item.prefab);
                objectsOnScene.Add(obg);
                obg.SetActive(false);
            }
        }
        Deadpanal.SetActive(false);
        StartGame.SetActive(true);
    }
    private void Update()
    {
        switch (faza)
        {
            case Faza.StartГейма:
                Deadpanal.SetActive(false);
                if (Input.anyKeyDown)
                {
                    
                    StartGame.SetActive(false);
                    Game = false;
                    faza = Faza.GameПлей;
                }

                break;

            case Faza.GameПлей:
                Game = true;
                Shoot.shoot.StartC();
                if(Enemy.EnemyGo >= 3)
                    faza = Faza.YourСмерть;

                break;

            case Faza.YourСмерть:
                
                Game = false;
                Deadpanal.SetActive(true);
                break;
            
        }

    }
    public GameObject Get(string teg,Vector3 v,int Forse)
    {
        foreach (GameObject item in objectsOnScene) 
        {
            if(item.CompareTag(teg) && item.activeInHierarchy == false && Game == true)
            {
                item.SetActive(true); 
                item.transform.position = v;
                item.GetComponent<Rigidbody>().isKinematic = false;
                item.GetComponent<Rigidbody>().AddForce(0, Forse, 0);
                return item;
            }
        }
        return null;
    }

    public GameObject Set(string teg)
    {
        foreach (GameObject item in objectsOnScene)
        {
            if (item.CompareTag(teg) && item.activeInHierarchy == true && Game == true)
            {
                item.SetActive(false);
                item.GetComponent<Rigidbody>().isKinematic = true;
                return item;
            }
        }
        return null;
    }

    public void Dead()
    {
        SceneManager.LoadScene(0);
        faza = Faza.StartГейма;
        Enemy.EnemyGo = 0;
    }

}
