﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    private Vector3 a;
    private float SpeedS = 4f;
    public static Shoot shoot;


    private void Awake()
    {
        if (shoot == null)
        {
            shoot = this;
        }
        else
        {
            Destroy(this);
        }

    }

    private void Update()
    {
        if(SpeedS >= 1.5f)
        SpeedS -= 0.05f * Time.deltaTime;


        if (Input.GetMouseButtonDown(0)) 
        {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
            RaycastHit Pos;
            if (Physics.Raycast(ray, out Pos))
            a = new Vector3(Pos.point.x, Pos.point.y, Pos.point.z); 
            Pool.Instanse.Get("bullet", a, 600); 
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            Pool.Instanse.Set("bullet");
           
        }
    }
    public void StartC()
    {
        StartCoroutine(AnemySpawn());
    }

    IEnumerator AnemySpawn()
    {
        while (true) 
        {
           float RandomX =  Random.Range(-5f, 5f);
           float RandomY =  Random.Range(6,5);

            Vector3 Spv = new Vector3(RandomX, RandomY, -2.5f); 
            Pool.Instanse.Get("enemy", Spv,0);
            yield return new WaitForSeconds(SpeedS);
        }
    }

}
